angular.module('myApp', ['ionic','myControllers','ui.router','ngCordova'])

.run(['$ionicPlatform', '$rootScope', function($ionicPlatform, $rootScope) {
  $ionicPlatform.ready(function() {

    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      //cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }

    // org.apache.cordova.statusbar required
    if (cordova.platformId == 'android') {
        StatusBar.backgroundColorByHexString("#2C3E50");
    }

  });
}])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	
	$stateProvider

    .state('menu', {
      url: '/menu',
      templateUrl: 'templates/menu.html',
      controller: 'Menu',
      abstract: true
    })

    .state('login', {
      url: '/login',
      templateUrl: 'templates/login.html',
      controller: 'Login'
    })

    .state('forgot-password', {
      url: '/forgot-password',
      templateUrl: 'templates/forgot-password.html',
      controller: 'forgotPassword'
    })

    .state('menu.route', {
      url: '/route',
      views: {
        menuContent: {
          templateUrl: 'templates/route.html',
          controller: 'Route',
        }
      }
    })

  $urlRouterProvider.otherwise('/login');

}])

.run(['$rootScope', function($rootScope) {

  //Scroll to top of page on view change so the user doesn't start a view in the middle of the page
    $rootScope.$on("$viewContentLoading",
        function (event) {
          $('body','html').animate({
              scrollTop: $('body','html')
          }, 0);

          //Show loading screen on route change
          $('.loading-overlay').fadeIn(0);
        });

    $rootScope.$on("$viewContentLoaded",
        function (event) {
            $('.loading-overlay').fadeOut(0);
        });

}])

//Filter double items from ng-repeat
.filter('unique', function() {
   return function(collection, keyname) {
      var output = [], 
          keys = [];

      angular.forEach(collection, function(item) {
          var key = item[keyname];
          if(keys.indexOf(key) === -1) {
              keys.push(key);
              output.push(item);
          }
      });

      return output;
   };
})

//Convert object to array
.filter('toArray', function () {
  return function (obj, addKey) {
    if (!angular.isObject(obj)) return obj;
    if ( addKey === false ) {
      return Object.keys(obj).map(function(key) {
        return obj[key];
      });
    } else {
      return Object.keys(obj).map(function (key) {
        var value = obj[key];
        return angular.isObject(value) ?
          Object.defineProperty(value, '$key', { enumerable: false, value: key}) :
          { $key: key, $value: value };
      });
    }
  }
})

//Set math round filter
.filter('round', function() {
    return function(input) {
        return Math.round(input);
    };
})

//Capitalize filter
.filter('capitalize', function() {
    return function(input) {
      return (!!input) ? input.charAt(0).toUpperCase() + input.substr(1).toLowerCase() : '';
    }
})

//Trusted sources
.filter('trusted', function ($sce) {
    return function(url) {
        return $sce.trustAsResourceUrl(url);
    };
})

// Start from filter (for pagination)
.filter('startFrom', function() {
    return function(input, start) {         
        return input.slice(start);
  }
})

//When a form changes directive
.directive("formOnChange", function($parse){
  return {
    require: "form",
    link: function(scope, element, attrs){
       var cb = $parse(attrs.formOnChange);
       element.on("change", function(){
          cb(scope);
       });
    }
  }
})

//Make the 'select' box work on mobile browsers
.directive('select',function(){ //same as "ngSelect"
    return {
        restrict: 'E',
        scope: false,
        link: function (scope, ele) {
            ele.on('touchmove touchstart',function(e){
                e.stopPropagation();
            })
        }
    }
})

;