angular.module('myControllers', [])

/* ---------- MENU ---------- */
.controller('Menu', ['$rootScope', '$scope', '$state', '$window', '$filter', 'orderByFilter', '$timeout', function($rootScope, $scope, $state, $window, $filter, orderBy, $timeout) {

  localStorage.setItem("sessieKenmerk", "");
  localStorage.setItem("batch", "");

	$rootScope.sessieKenmerk = localStorage.getItem('sessieKenmerk');
  $rootScope.batch = localStorage.getItem('batch');
  $rootScope.batchOptiesActive = false;


	//Help button function
	$scope.showHelp = function() {
		swal({
		  title: 'Hulp',
		  type: 'question',
		  html: 'Heeft u hulp nodig bij het gebruik van de app? Raadpleeg dan onze handleiding. U kunt te allen tijde de handleiding raadplegen door linksboven in de app op de <span style="color:#fff;background:#0f7366;font-weight:bold;border-radius:100%;width:28px;height:25px;padding-top:2.5px;display:inline-block;">?</span> knop te klikken.',
		  confirmButtonColor: '#009999',
		  showCancelButton: true,
		  cancelButtonText: 'Sluiten',
		  confirmButtonText: 'Handleiding'
		}).then(function() {
			window.open("https://bvobaarmoederhals.nl/files/Handleiding-Commpanionz-App.pdf",'_blank');
		});

		//Set help message to 'true' in local storage so it won't automatically show on login
		localStorage.setItem('CommpanionzAppHelp',true);
	}

	/*
	//Show help on first login
	if (localStorage.getItem('CommpanionzAppHelp') === null) {
		$scope.showHelp();
	} else {
		//Do nothing if 'CommpanionzAppHelp' already exists in local storage, meaning it has already been shown before
	}*/

	//Check if user is logged in
	if ($rootScope.sessionData && $rootScope.loggedInUserId && $rootScope.loggedInUserToken) {

		//Get logged in user data
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetSingleUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {

					$timeout(function() {

						//Logged in user object
						$rootScope.loggedInUserObject = response.users;

						//Get logged in user role
						$rootScope.role = response.users.role;

						//Get logged in user name
						$rootScope.username = response.users.identity;

						//Get logged in user image
						$rootScope.profileImageCurrentUser = response.users.image;

						//Get logged in user route
						if ($rootScope.role == 'huisartsenpraktijk' || $rootScope.role == 'koerier') {

							//Get list of routes that are connected to the user
							$rootScope.loggedInUserRouteList = response.users.routeId;

							//Functions to execute once all routes are loaded
							$rootScope.$watch('routesDetails', function (newValue, oldValue, scope) {
								//Add route names and default drive dates to the user route list
								for (var r in $rootScope.loggedInUserRouteList) {
									for (var ro in $rootScope.routesDetails) {
										if ($rootScope.routesDetails[ro].routeId == $rootScope.loggedInUserRouteList[r].id) {
											$rootScope.loggedInUserRouteList[r]['routeName'] = $rootScope.routesDetails[ro].routeNaam;
											$rootScope.loggedInUserRouteList[r]['vasteDag'] = $rootScope.routesDetails[ro].vasteDag;
										}
									}
								}

								//Get the route that is driven today (by getting the visit day and comparing it with today (day as string)).
								//Set $rootScope variable for that route.
								//(only for koerier)
								if ($rootScope.role == 'koerier') {
									for (var route in $rootScope.loggedInUserRouteList) {
										if ($rootScope.getWeekDay(new Date()) == $rootScope.loggedInUserRouteList[route].vasteDag) {
											$rootScope.loggedInUserRoute = $rootScope.loggedInUserRouteList[route].id;
										}
									}
								}
								//Set route for huisartsenpraktijk to the first route in the users' routelist array
								if ($rootScope.role == 'huisartsenpraktijk') {
									$rootScope.loggedInUserRoute = $rootScope.loggedInUserRouteList[0].id;
								}
							});

							if ($state.current.name == 'menu.route') {
								//Preselect current date
								$scope.currentDate = new Date($.date(Date.now()).replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") );
								//Click on date in calender
								$timeout(function() {
									$('.datepickerDays').find('span:contains("'+$scope.currentDate.getDate()+'")').parent('a').each(function() {
										if ($(this).parent().hasClass('datepickerNotInMonth')) {
											//Do nothing
										} else {
											$(this).click();
										}
									});
								});
							}
						}

						//Get logged in user region
						$rootScope.loggedInUserRegion = response.users.regionId;

						//Get logged in user URL
						$rootScope.loggedInUserUrl = response.users.url;

						//Get logged in address
						$rootScope.huisartsenpraktijkDataAddress = response.users.address;

						//Get logged in city
						$rootScope.huisartsenpraktijkDataCity = response.users.city;

						//Get logged in postcode
						$rootScope.huisartsenpraktijkDataPostcode = response.users.postcode;

						//Get logged in phone
						$rootScope.huisartsenpraktijkDataTelephone = response.users.telephone;

						//Get logged in email
						$rootScope.huisartsenpraktijkDataEmail = response.users.email;

						//Get place in route
						$rootScope.loggedInUserRoutePlace = response.users.routePlace;

						//Get all regions
						$rootScope.getRegions();

						//Get all routes
						$rootScope.getRoutes();

						//Get all roles
						$rootScope.getRoles();

						//Get all users
						$.ajax({
							method:'POST',
							dataType:'json',
							data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:'',roleId:''}}}),
							url:$rootScope.ajaxURL,
							success: function(response) {

								if (response.state.result == 'Fail') {
									swal({
									  title: 'Oops...',
									  text: response.state.message,
									  type: 'error',
									  confirmButtonColor: '#009999'
									});

									if (response.state.message == 'Session Expired') {
										$rootScope.removeSession();
									}

								} else if (response.state.result == 'Success') {
									$timeout(function() {
										$rootScope.usersDetails = response.users;
									});

								} else if (response.state.result == 'Empty') {
									swal({
									  title: 'Geen resultaten',
									  text: 'Er zijn geen resultaten gevonden...',
									  type: 'warning',
									  confirmButtonColor: '#009999'
									});
								}
							},
							error: function(response) {
								console.log(response);
							}
						});
						//End get all users

						//Get all unread meldingen
						$rootScope.getUnreadMeldingen();

						//Load all user changes (adreswijzigingen) if logged in user is 'beheerder'
						if ($rootScope.role == 'beheerder') {
							$rootScope.getAllUserChanges();
						}

						//Get all type meldingen
						$rootScope.getTypeMeldingen();

						//Get all klacht reasons
						$rootScope.getKlachtReasons();

						//Get all kittypes
						$rootScope.getKitTypes();

						//Get no materiaal opgehaald reasons
						$rootScope.getAllPlanningGeenMateriaalReden();

						//Get all verzendmethodes
						$rootScope.getAllPlanningVerzendMethodes();

						//Get reasons for Bezemwagen as verzendmethode
						$rootScope.getReasonsBezemwagen();

						//Get reasons for Spoedenvelop as verzendmethode
						$rootScope.getReasonsSpoedenvelop();

					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});

	} else {
		//If not logged in, navigate to login page
		$state.go('login');
	}

	//Log out user
	$('.logout-button').click(function() {
		$rootScope.removeSession();
	});

	//Change view title in top bar based on controller title
	$scope.dynamicActiveView = function() {
		$rootScope.viewTitle = $state.current.views.menuContent.controller;
	};

	//Do function when arriving on page
	$scope.dynamicActiveView();

	//Do function whenwhen state changes
	$rootScope.$on('$stateChangeSuccess', function(){
		$scope.dynamicActiveView();
	});

	$('.logged-in-user').click(function() {
		$('.logout-container').addClass('animated fadeInUp');
		$('.logout-container').removeClass('hide');
		$('.logout-overlay').fadeIn(0);
		$('.menuContent').removeClass('nativeScroll');
	});

	$('.close-logout').click(function() {
		$('.logout-container').removeClass('animated fadeInUp');
		$('.logout-container').addClass('hide');
		$('.logout-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	});

	$('.logout-overlay').click(function() {
		$('.logout-container').removeClass('animated fadeInUp');
		$('.logout-container').addClass('hide');
		$('.logout-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	});

	$('.logout-button').click(function() {
		$('.logout-container').removeClass('animated fadeInUp');
		$('.logout-container').addClass('hide');
		$('.logout-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	});


	/*Toggle hamburger menu
	$('.menu').click(function() {
		$('.menu').animate({
	        scrollTop: $('.menu ')
	    }, 0); //scroll to top of menu element because overflow is scroll, sometimes it will be scrolled down a bit
		$('.menu').toggleClass('full-height');
	});*/

}])































/* ---------- MAIN CONTROLLER ---------- */
.controller('MainCtrl', ['$rootScope', '$scope', '$state', '$timeout', '$filter', function($rootScope, $scope, $state, $timeout, $filter) {

	//Get ajax URL
	$rootScope.ajaxURL = 'https://bvobaarmoederhals.nl/backend/index.php';

	//Test url (staging url)
	//$rootScope.ajaxURL = 'https://bvobaarmoederhals.nl/staging/index.php';

	//Setup function for every AJAX request
	$.ajaxSetup({
	    beforeSend: function() {
	        //Show loading screen
			$('.loading-overlay').fadeIn(0);
	    },
	    complete: function() {
	        //Hide loading screen
			$('.loading-overlay').fadeOut(0);
	    }
	});

	//Get session data
	$rootScope.getSessionData = function() {
		if (localStorage.getItem('CommpanionzAppSessionData') === null) {
			$rootScope.loggedInUserId = null;
			$rootScope.loggedInUserToken = null;
		} else {
			$rootScope.sessionData = JSON.parse(localStorage.getItem('CommpanionzAppSessionData'));
			$rootScope.loggedInUserId = $rootScope.sessionData.userId;
			$rootScope.loggedInUserToken = $rootScope.sessionData.token;
		}
	}

	//Call get session data function
	$rootScope.getSessionData();

	//Remove session data
	$rootScope.removeSession = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"DestroySession",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				$rootScope.sessionData = null;
				$rootScope.loggedInUserId = null;
				$rootScope.loggedInUserToken = null;
				localStorage.removeItem('CommpanionzAppSessionData');
				location.reload();

				if (response.state.result == 'Fail') {
					//Sessie verwijderen mislukt

				} else if (response.state.result == 'Success') {
					//Sessie verwijderd
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End remove session data

	//Get all regions
	$rootScope.getRegions = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetRegions",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.regionsDetails = response.regions;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End get all regions

	//Get all roles
	$rootScope.getRoles = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetRoles",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.rolesDetails = response.roles;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End get all roles

	//Get all routes
	$rootScope.getRoutes = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetRoutes",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.routesDetails = response.routes;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End get all routes

	//Get all users (based on filter)
	$rootScope.getUsers = function(regioId, routeId, roleId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:regioId,routeId:routeId,roleId:roleId}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.usersDetails = response.users;
					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen resultaten gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}// End get all users (based on filter)

	//Get user details
	$rootScope.getSingleUser = function(singleUserId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetSingleUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:singleUserId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

					//Reset singleUserDetails if no result
					$timeout(function() {
						$scope.singleUserDetails = '';
					});

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.singleUserDetails = response.users;

						//Get huisartsen if user is huisartsenpraktijk
						if ($scope.singleUserDetails.role == 'huisartsenpraktijk') {
							$rootScope.getHuisartsen($scope.singleUserDetails.user_id);
						}
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get user details

	//Get user huisartsen
	$rootScope.getHuisartsen = function(huisartsId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetHuisartsen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{huisartsId:huisartsId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

					//Reset singleUserDetails if no result
					$timeout(function() {
						$scope.singleUserDetails = '';
					});

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.huisartsen = response.huisartsen;
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get user huisartsen

	//Set user huisartsen
	$rootScope.setHuisartsen = function(huisartsId,huisartsen) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"SetHuisartsen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{huisartsId:huisartsId,huisartsen:huisartsen}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

					//Reset singleUserDetails if no result
					$timeout(function() {
						$scope.singleUserDetails = '';
					});

				} else if (response.state.result == 'Success') {
					//Huisartsen zijn toegevoegd (functie wordt gecombineerd met updateUser, succesmelding komt daarvandaan)
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End set user huisartsen

	//Update user
	$rootScope.updateUser = function(userId,email,oldemail,roleId,regionId,identity,name,lastname,routeId,routePlace,image,address,postcode,city,telephone,standaardWaarde,url,notitie,password,newpassword) {

		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdateUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,email:email,oldemail:oldemail,roleId:roleId,regionId:regionId,identity:identity,name:name,lastname:lastname,routeId:routeId,routePlace:routePlace,image:image,address:address,postcode:postcode,city:city,telephone:telephone,standaardWaarde:standaardWaarde,url:url,notitie:notitie,password:password,newpassword:newpassword}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'De wijzigingen zijn doorgevoerd. Indien u geen beheerdersrol hebt zullen eventuele adreswijzigingen eerst door een beheerder moeten worden goedgekeurd.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End update user

	//Get all adreswijzigingen
	$rootScope.getAllUserChanges = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUpdateRequests",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.userUpdates = response.data.updates;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get all adreswijzigingen

	//Get all type messages
	$rootScope.getTypeMeldingen = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetTypeMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.typeMeldingen = response.meldingen;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get all type messages

	//Get all klacht reasons
	$rootScope.getKlachtReasons = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetRedenKlacht",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.klachtReasons = response.redenKlacht;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get all klacht reasons

	//Get total amount of unread notifications
	$rootScope.calculateUnread = function() {
		$rootScope.getUnreadMeldingen();
	}

	//Get unread meldingen
	$rootScope.getUnreadMeldingen = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUnreadMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.unreadMessages = response.state.count;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}
	//End get unread meldingen

	//Send message
	$rootScope.sendMessage = function(userId,typemelding,bericht,anders,redenklacht,klacht_user_id,klacht_van_user_id,receivers,routeId2send,regionId2send,roleId2send,sendAllUsers) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"CreateMelding",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{ontvangers:receivers,userId:userId,typemelding:typemelding,bericht:bericht,anders:anders,redenklacht:redenklacht,klacht_user_id:klacht_user_id,klacht_van_user_id:klacht_van_user_id,routeId2send:routeId2send,regionId2send:regionId2send,roleId2send:roleId2send,sendAllUsers:sendAllUsers}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'Melding is verstuurd.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});

					//Reload all sent messages (if they were already loaded)
					if ($rootScope.mySentMessages) {

						//Load dates now and three months ago, so messages can be downloaded from that period
						var currentDateMeldingen = $.reverseDate(Date.now());
						var d = new Date();
						var threeMonthsAgoDateMeldingen = $.reverseDate(d.setMonth(d.getMonth() - 3));

						$.ajax({
							method:'POST',
							dataType:'json',
							data:'data=' + JSON.stringify({init:{module:"GetMeldingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{zender:$rootScope.loggedInUserId,ontvanger:'',routeId:'',regionId:'',datum:'',startdatum:threeMonthsAgoDateMeldingen,einddatum:currentDateMeldingen,klacht_user_id:'',klacht_van_user_id:'',typemelding:'',meldingId:'',ShowOntvangers:''}}),
							url:$rootScope.ajaxURL,
							success: function(response) {

								if (response.state.result == 'Fail') {
									swal({
									  title: 'Oops...',
									  text: response.state.message,
									  type: 'error',
									  confirmButtonColor: '#009999'
									});

									if (response.state.message == 'Session Expired') {
										$rootScope.removeSession();
									}

								} else if (response.state.result == 'Success') {
									$timeout(function() {
										$rootScope.mySentMessages = response.data.meldingen;
										$rootScope.mySentMessagesFiltered = $rootScope.mySentMessages;
									});
								}
							},
							error: function(response) {
								console.log(response);
							}
						});
					}

					$('.send-message-overlay').fadeOut(0);
					$('.menuContent').addClass('nativeScroll');
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End send message

	//Get kittypes
	$rootScope.getKitTypes = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetKits",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.kitTypes = response.kits;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get kittypes

	//Get all bestellingen for user on selected date
	$rootScope.getAllUserBestellingen = function(datum, userId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetBestellingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,datum:datum}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.userBestellingen = response.bestellingen;
					});

				} else if (response.state.result == 'Empty') {
					$timeout(function() {
						$rootScope.userBestellingen = '';
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get all bestellingen for user on selected date

	//Delete specific bestelling for user on selected date
	$rootScope.deleteSelectedBestelling = function(bestellingId) {

		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"DeleteBestelling",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{bestellingId:bestellingId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: response.state.message,
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//Delete specific bestelling for user on selected date

	//Set aanwezigheid for selected date and user
	$rootScope.setUserDateAanwezigheid = function(userId,datum,afwezig) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"SetAfwezigheid",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,datum:datum,afwezig:afwezig}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: response.state.message,
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End set aanwezigheid for selected date and user

	//Get aanwezigheid for selected date and user
	$rootScope.getUserDateAanwezigheid = function(userId,datum) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetAanwezig",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,datum:datum}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.userAanwezigheid = '0';
						$scope.praktijkAanwezigheid = response.aanwezigheid[0].afwezig;
						//Change select field to new value on 'Planning' page
						//$('#planningAanwezigheidSelect').val($scope.userAanwezigheid).trigger('change');
					});
				} else if (response.state.result == 'Empty') {
					$timeout(function() {
						$scope.userAanwezigheid = '0';
						delete $scope.praktijkAanwezigheid;
						//Change select field to new value on 'Planning' page
						//$('#planningAanwezigheidSelect').val($scope.userAanwezigheid).trigger('change');
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get aanwezigheid for selected date and user

	//Get planning
	$rootScope.getPlanning = function(startdatum,einddatum,routeId,planningId) {

		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{startdatum:startdatum,einddatum:einddatum,routeId:routeId,planningId:planningId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.planningDetails = response.planning;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get planning

	//Save planning
	$rootScope.updatePlanning = function(planningId,datum,routeId,praktijkId,afwijkendeVerzendmethode,afwijkendeVerzendmethodeReason,huisartsenpraktijk,bestellingen,materiaal_opgehaald,kits_afgeleverd,noMateriaalGivenReason,anders,signatureKit,signatureMateriaal,koerier_id,koerier,notitie,regionId) {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdatePlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{planningId:planningId,datum:datum,routeId:routeId,praktijkId:praktijkId,afwijkendeVerzendmethode:afwijkendeVerzendmethode,afwijkendeVerzendmethodeReason:afwijkendeVerzendmethodeReason,huisartsenpraktijk:huisartsenpraktijk,bestellingen:bestellingen,materiaal_opgehaald:materiaal_opgehaald,kits_afgeleverd:kits_afgeleverd,noMateriaalGivenReason:noMateriaalGivenReason,anders:anders,signatureKit:signatureKit,signatureMateriaal:signatureMateriaal,koerier_id:koerier_id,koerier:koerier,notitie:notitie,regio:regionId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					//Succes message
					swal({
					  title: 'Gelukt!',
					  text: 'De gegevens zijn opgeslagen.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//save

	//Get verzendmethodes
	$rootScope.getAllPlanningVerzendMethodes = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanningVerzendMethodes",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.verzendmethodes = response.verzendmethodes;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get verzendmethodes

	//Get no materiaal opgehaald reasons
	$rootScope.getAllPlanningGeenMateriaalReden = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanningGeenMateriaalReden",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.geenmateriaalreden = response.geenmateriaalreden;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get no materiaal opgehaald reasons

	//Get reasons for Bezemwagen
	$rootScope.getReasonsBezemwagen = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanningBezemwagen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.bezemwagen = response.bezemwagen;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get reasons for Bezemwagen

	//Get reasons for Spoedenvelop
	$rootScope.getReasonsSpoedenvelop = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetPlanningSpoedEnvelop",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$rootScope.spoedenvelop = response.spoedenvelop;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get reasons for Spoedenvelop

	//Delete notitie
    $rootScope.deleteNotitie = function(userId) {
    	$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"DeleteNotitie",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{UserIdNotitie:userId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {
				console.log(response);

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'De notitie is verwijderd!',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});
				}

			},
			error: function(response) {
				console.log(response);
			}
		});
    }//End delete notitie

	//Update/insert barcode in database
	$rootScope.insertBarcode = function(event,RouteId,praktijkId,barCode,scanKoerierDatum,koerierScan,laboratoriumScanDatum,laboratoriumScan,kitType,sessieKenmerk,batchKenmerk) {

		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdateScans",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{RouteId:RouteId,praktijkId:praktijkId,barCode:barCode,scanKoerierDatum:scanKoerierDatum,koerierScan:koerierScan,laboratoriumScanDatum:laboratoriumScanDatum,laboratoriumScan:laboratoriumScan,kitType:kitType,sessieKenmerk:sessieKenmerk,batchKenmerk:batchKenmerk}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {

					// swal({
					//   title: 'Gelukt!',
					//   text: 'Barcode is gescand en ingevoerd!',
					//   type: 'success',
					//   confirmButtonColor: '#009999'
					// });

					//Submit planning after scan if user is koerier
					if ($rootScope.role == 'koerier' || $rootScope.role == 'beheerder') {
						$timeout(function() {
							//Set 'materiaal opgehaald' to yes (= ja, (value '1'))
							$(event).parent().find('.materiaalOpgehaaldSelection').val('1').change();

							//Save route details
							$(event).parent().find('.planning-form').find('button[type="submit"]').click();

							//Update scan count
							var changeScannedAmount = parseInt($(event).parent().find('.scannedMonstersKoerier').text());
							$(event).parent().find('.scannedMonstersKoerier').text(changeScannedAmount + 1);
						});
					}
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}

	//Convert profile image to Base64 for storing it as text in database
    File.prototype.convertToBase64 = function(callback){
        var reader = new FileReader();
        reader.onload = function(e) {
             callback(e.target.result)
        };
        reader.onerror = function(e) {
             callback(null);
        };
        reader.readAsDataURL(this);
    };

	//Restrict image upload size on file inputs
    $(document).unbind().on('change', 'input[type=file]', function(event){
    	var fileTarget = event.currentTarget;
    	var selectedFile = fileTarget.files[0];
    	if(typeof fileTarget.files[0] !== 'undefined'){
            var maxSize = parseInt($(this).attr('max-size'),10);
            var size = fileTarget.files[0].size;
            if (maxSize > size) {
            	//If size is accepted, convert to base64 and show to user in .profile-image-container
            	selectedFile.convertToBase64(function(base64){
	        		$('.profile-image-container').css('background', 'url('+base64+')');
	        		$timeout(function() {
	        			$rootScope.profileImageInput = base64;
	        		});
	      		});
            } else {
            	//File size is not accepted
            	swal({
				  title: 'Oops...',
				  text: 'De foto mag maximaal maar 1mb groot zijn.',
				  type: 'error',
				  confirmButtonColor: '#009999'
				});
				//Reset file input
				$(fileTarget).val('');
				$('.profile-image-container').css('background','transparent');
            }
        } else {
        	//Reset file input
        	$(fileTarget).val('');
			$('.profile-image-container').css('background','transparent');
        }
    });

    //Convert date to DD-MM-YYYY
	$.date = function(dateObject) {
	    var d = new Date(dateObject);
	    var day = d.getDate();
	    var month = d.getMonth() + 1;
	    var year = d.getFullYear();
	    var hours = d.getHours();
	    var minutes = d.getMinutes();
	    if (day < 10) {
	        day = "0" + day;
	    }
	    if (month < 10) {
	        month = "0" + month;
	    }

	    var dateString = day + "-" + month + "-" + year;
	    return dateString;
	};

	//Convert date to YYYYMMDD (for database)
	$.reverseDate = function(dateObject) {
	    var d = new Date(dateObject);
	    var day = d.getDate();
	    var month = d.getMonth() + 1;
	    var year = d.getFullYear();
	    var hours = d.getHours();
	    var minutes = d.getMinutes();
	    if (day < 10) {
	        day = "0" + day;
	    }
	    if (month < 10) {
	        month = "0" + month;
	    }

	    var reverseDateString = year + "" + month + "" + day;
	    return reverseDateString;
	};

	//Convert YYYYMMDD to date object
	$rootScope.convertDatabaseDate = function(dateString) {
		var dateString  = dateString;
		var year = dateString.substring(0,4);
		var month = dateString.substring(4,6);
		var day = dateString.substring(6,8);

		var outputDate = $.date(new Date(year, month-1, day));

		return outputDate;
	}

    //Get days of the week
    $rootScope.getWeekDay = function(date) {
		var weekday = new Array(7);
		weekday[0]=  "zondag";
		weekday[1] = "maandag";
		weekday[2] = "dinsdag";
		weekday[3] = "woensdag";
		weekday[4] = "donderdag";
		weekday[5] = "vrijdag";
		weekday[6] = "zaterdag";

		return weekday[date.getDay()];
	}

	//Custom jQuery scroll to position function
	jQuery.fn.scrollTo = function(elem) {
	    $(this).scrollTop($(this).scrollTop() - $(this).offset().top + $(elem).offset().top);
	    return this;
	}

}])






























/* ---------- NAVIGATION CONTROLLER ---------- */
.controller('NavigationCtrl', ['$scope', '$location', function ($scope, $location) {
    //Automatically select menu item that is the current page
    $scope.isCurrentPath = function (path) {
      return $location.path() == path;
    };
}])



































/* ---------- LOGIN ---------- */
.controller('Login', ['$rootScope', '$scope', '$state', '$timeout', function($rootScope, $scope, $state, $timeout) {

	//If already logged in, go to route page
	if ($rootScope.sessionData && $rootScope.loggedInUserId && $rootScope.loggedInUserToken) {
		$state.go('menu.route');
	}

	//Login form function being called on login.html
	$scope.login = function() {

		//Check if all fields are filled, if not:
		if(!$('#login-form input').val()) {
			swal({
			  title: 'Hmm...',
			  text: 'Je hebt niet alle velden ingevuld.',
			  type: 'warning',
			  confirmButtonColor: '#009999'
			});

		//If all fields are filled:
		} else {

			//Set variables for login
			var email = $('#email').val();
			var password = $('#password').val();

			//Call login function from backend
			$.ajax({
				method: 'POST',
				data: 'data=' + JSON.stringify({init:{module:"login",email:email,password:password}}),
				url: $rootScope.ajaxURL,
				dataType: 'json',
				success: function(response) {

					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});
					} else if (response.state.result == 'Success') {
						swal({
						  title: 'Gelukt!',
						  text: response.state.message,
						  type: 'success',
						  confirmButtonColor: '#009999'
						});

						//Set user details
						localStorage.setItem('CommpanionzAppSessionData',JSON.stringify(response.sessiondata));

						//Get user data
						$rootScope.getSessionData();

						//Go in app
						$state.go('menu.route');
					}

				},
				error: function(response) {
					console.log(response);
				}
			});//End call login function from backend
		};
	}
}])





















/* ---------- FORGOT PASSWORD ---------- */
.controller('forgotPassword', ['$scope', '$rootScope', '$state', function($scope, $rootScope, $state) {

	$scope.forgotPassword = function(event) {
		//Check if emailaddress was filled in
		if(!$('#forgot-password-form input').val()) {
			swal({
			  title: 'Hmm...',
			  text: 'Je hebt niet alle velden ingevuld.',
			  type: 'warning',
			  confirmButtonColor: '#009999'
			});

		} else {

			//Remove button to prevent second press
			$(event.currentTarget).remove();

			//Hide loading screen
			$('.loading-overlay').fadeIn(0);

			var emailAddress = $('#forgot-password-form input').val();

			$.ajax({
				method:'POST',
				data:'data=' + JSON.stringify({init:{module:"GetNewPassword",email:emailAddress}}),
				url:$rootScope.ajaxURL,
				dataType:'json',
				success: function(response) {
					//Hide loading screen
					$('.loading-overlay').fadeOut(0);



					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});
					} else if (response.state.result == 'Success') {
						swal({
						  title: 'Gelukt!',
						  text: response.state.message,
						  type: 'success',
						  confirmButtonColor: '#009999'
						});

						//Go back to login
						$state.go('login');
					}
				},
				error: function(response) {
					//Hide loading screen
					$('.loading-overlay').fadeOut(0);
					console.log(response);
				}
			});
		}
	}

}])


































/* ---------- ROUTE ---------- */
.controller('Route', ['$scope', '$timeout', '$rootScope', '$filter', '$cordovaBarcodeScanner', '$cordovaLaunchNavigator', function($scope, $timeout, $rootScope, $filter, $cordovaBarcodeScanner, $cordovaLaunchNavigator) {

	//Get users with selected route
	$scope.getUsersAndPlanningWithRoute = function(routeId) {

		//Users from route
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:routeId,roleId:''}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.usersDetailsRouteNoFilter = response.users;

						//Get aanwezigheid for selected date
						$.ajax({
							method:'POST',
							dataType:'json',
							data:'data=' + JSON.stringify({init:{module:"GetAanwezig",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:'',datum:$scope.dataBaseDate}}),
							url:$rootScope.ajaxURL,
							success: function(response) {

								if (response.state.result == 'Success' || response.state.result == 'Empty') {
									$timeout(function() {
										$scope.userAanwezigheid = response.aanwezigheid;

										for (var i in $scope.usersDetailsRouteNoFilter) {
											for (var j in $scope.userAanwezigheid) {
												if ($scope.userAanwezigheid[j].userId == $scope.usersDetailsRouteNoFilter[i].user_id) {
													//Set user afwezigheid in users array
													$scope.usersDetailsRouteNoFilter[i]['afwezig'] = $scope.userAanwezigheid[j].afwezig;
													break;
												} else {
													//Set user afwezigheid to null if doesn't exists in aanwezigheidstabel
													$scope.usersDetailsRouteNoFilter[i]['afwezig'] = null;
												}
											}
										}//End loop

										//Check aanwezigheid for each user in users array
										var afwezig = '1';
										var aanwezig = '0';
										var standaardWaarde = 'nee';
										var roleKoerier = 'koerier';

										for(var i = 0; i < $scope.usersDetailsRouteNoFilter.length; i++) {
										    var obj = $scope.usersDetailsRouteNoFilter[i];

										    //Remove user from array if set to 'afwezig' or if 'standaardWaarde' is 'nee' and user is not set to aanwezig (afwezig = 0), excluding 'koeriers'
										    if(((afwezig.indexOf(obj.afwezig) !== -1) || (standaardWaarde.indexOf(obj.standaardWaarde) !== -1 && aanwezig.indexOf(obj.afwezig) == -1)) && roleKoerier.indexOf(obj.role) == -1) {
										        $scope.usersDetailsRouteNoFilter.splice(i, 1);
										        i--;
										    }
										}

										//Set new variable for route
										$timeout(function() {
											$scope.usersDetailsRoute = $scope.usersDetailsRouteNoFilter;
										});

										//Reset pagination to first page
										$scope.currentPage = 0;
									});
								}

							},
							error: function(response) {
								console.log(response);
							}
						});//End Get aanwezigheid

					});

				} else if (response.state.result == 'Empty') {
					swal({
					  title: 'Geen resultaten',
					  text: 'Er zijn geen resultaten gevonden...',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});

					//Reset values, so the old data doesn't still show
					$timeout(function() {
						$rootScope.planningDetails = '';
						$scope.usersDetailsRoute = '';
						$scope.userAanwezigheid = '';
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});

		//Get planning
		$rootScope.getPlanning($scope.dataBaseDate,'',routeId,'');
	}

	//Automatically load the route for the current day for huisartsen and koeriers if a route they are connected to is driven on the same day as the current day
	//This will be executed once the '$rootScope.loggedInUserRoute' has been loaded before manually selecting a route
	$rootScope.$watch('loggedInUserRoute', function (newValue, oldValue, scope) {
		if (!$scope.usersDetailsRouteNoFilter && $rootScope.loggedInUserRoute) {
			$scope.getUsersAndPlanningWithRoute($rootScope.loggedInUserRoute);
		}
	});

	//Route pagination
	$scope.currentPage = 0;
	$scope.pageSize = 10;

	$scope.setCurrentPage = function(event,currentPage) {
	    $scope.currentPage = currentPage;
	    $('.pagination-button').removeClass('active');
		$(event.currentTarget).addClass('active');

		//Scroll back to top of route
		$(".menuContent").scrollTo("#top-of-route");
	}
	$scope.getNumberAsArray = function (num) {
	    return new Array(num);
	};
	$scope.numberOfPages = function() {
		var targetRole = 'koerier';
		var roleCounter = 0;

		//Get amount or koeriers and exclude them from pagination, since they do not have to be visited
		for(var i = 0; i < $scope.usersDetailsRoute.length; i++) {
		    var obj = $scope.usersDetailsRoute[i];
		    if((targetRole.indexOf(obj.role) !== -1)) {
		    	roleCounter += 1;
		    }
		}

	    return Math.ceil(($scope.usersDetailsRoute.length - roleCounter) / $scope.pageSize);
	};

	//Preselect current date
	$scope.currentDate = new Date($.date(Date.now()).replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") );
	//Click on date in calender
	$timeout(function() {
		$('.datepickerDays').find('span:contains("'+$scope.currentDate.getDate()+'")').parent('a').each(function() {
			if ($(this).parent().hasClass('datepickerNotInMonth')) {
				//Do nothing
			} else {
				$(this).click();
			}
		});
	});

	//Call datepicker on date-select input from datepicker.js
	$('#date-select-route').DatePicker({
		flat: true,
		date: new Date(),
		current: new Date(),
		format: 'd-m-Y',
		calendars: 1,
		starts: 1,
		onChange: function(date) {


			$timeout(function() {

				//Selected date
				$scope.chosenDate = new Date(date.replace( /(\d{2})-(\d{2})-(\d{4})/, "$2/$1/$3") );

				//Get day of selected date
				$scope.selectedDateDay = $rootScope.getWeekDay($scope.chosenDate);

				//Get weeknumber of selected date
				$scope.selectedWeekNumber = $filter('date')($scope.chosenDate, 'ww');

				//Get current weeknumber
				$scope.currentWeekNumber = $filter('date')(new Date(), 'ww');

				//Show warning if selected date is not in the current week
				if ($scope.selectedWeekNumber != $scope.currentWeekNumber) {
					swal({
					  title: 'Let op!',
					  text: 'De geselecteerde datum valt niet binnen de huidige week.',
					  type: 'warning',
					  confirmButtonColor: '#0f7366'
					});
				}

				//Databasedate
				$scope.dataBaseDate = $.reverseDate($scope.chosenDate);



        // ========== Bekijken items onder sessiekenmerk en batch en mogelijkheid om batch te verwijderen ==========
        $scope.editSessionBatch = function() {
          if ($rootScope.batch !== "") {
            $("#huidigeSessie").toggleClass("active");
            $(".material-icons").toggle();

            if ($rootScope.batchOptiesActive) {
              $rootScope.batchOptiesActive = false;
            } else {
              $rootScope.batchOptiesActive = true;
            }
          }

        }

        $scope.verwijderBatch = function(){
          localStorage.removeItem("batch");
          $rootScope.batch = '';
          $rootScope.batchOptiesActive = false;
          $("#huidigeSessie").toggleClass("active");
        }


				// ========== Scan kit or monsters functions ==========
				$scope.scanKitOrMonsters = function(event, routeId, userId) {
          //Function can not be used if the user is not a koerier or laboratorium
          //Volgende versie app: Laboratoriumfunctie eruit halen!
          if ($rootScope.role != "koerier" && $rootScope.role != "laboratorium" && $rootScope.role != "beheerder") {
            swal({
              title: "Oops...",
              text:
                "U kunt deze functie niet gebruiken. Alleen koeriers en beheerders hebben deze bevoegdheid.",
              type: "error",
              confirmButtonColor: "#009999"
            });
          } else {
            //If the user is a koerier or laboratorium:

            //Set variables
            $scope.scanMonsterRouteId = routeId;
            $scope.scanMonsterUserId = userId;
            $scope.scanMonsterElement = event.currentTarget;

            /*
							=========================================
									MELDING MISSENDE MONSTERS
							=========================================
						*/

            //Check if there is already a scan-monster-session in the local storage
            if (localStorage.getItem("scannedMonstersRoutesCommpanionz") !== null) {
              //Set scanned monsters routes as variable
              var scannedMonstersRoutes = JSON.parse(localStorage.getItem("scannedMonstersRoutesCommpanionz"));
              //Set scan session to true
              $timeout(function() {
                $scope.scanSession = true;
              });
            } else {
              //If no session exists yet, create new array for scanned monsters routes as variable
              var scannedMonstersRoutes = [];
              //Set scan session to false
              $timeout(function() {
                $scope.scanSession = false;
              });
            }

            /*
							=========================================
								EINDE MELDING MISSENDE MONSTERS
									(wordt verderop vervolgd)
							=========================================
						*/

            //Start scanner
            $cordovaBarcodeScanner.scan().then(function(barcodeData) {
                //If success

                //Check if scanned barcode is Sessie barcode
                if (barcodeData.text.indexOf("SES") !== -1) {
                  localStorage.removeItem("sessieKenmerk");
                  localStorage.removeItem("batch");
                  $rootScope.batch = "";
                  // var barcodeStringify = JSON.stringify(barcodeData.text);
                  localStorage.setItem("sessieKenmerk", barcodeData.text);
                  var sessieKenmerk = localStorage.getItem("sessieKenmerk");
                  $rootScope.sessieKenmerk = localStorage.getItem("sessieKenmerk");

                  // 		var sessiekenmerkstorage = localStorage.getItem('sessieKenmerk');

                  swal({
                    title: "Sessie ingesteld!",
                    text: sessieKenmerk,
                    type: "success",
                    confirmButtonColor: "#009999"
                  });
                  $scope.$apply();
                } else if (barcodeData.text.indexOf("BATCH") !== -1) {
                  // check of een sessie is ingesteld
                  if (localStorage.getItem("sessieKenmerk") == "") {
                    swal({
                      title: "Oops...",
                      text:
                        "Scan eerst een sessiecode. Scan de 'BATCH'-code erna opnieuw",
                      type: "error",
                      confirmButtonColor: "#009999"
                    });
                  } else {
                    localStorage.removeItem("batch");
                    localStorage.setItem("batch", barcodeData.text);
                    var batchKenmerk = localStorage.getItem("batch");
                    $rootScope.batch = batchKenmerk;
                    swal({
                      title: "Batch ingesteld!",
                      text: batchKenmerk,
                      type: "success",
                      confirmButtonColor: "#009999"
                    });
                    $scope.$apply();
                  }

                  //Check if scanned barcode is for Commpanionz app

                }else if (barcodeData.text.indexOf("2S") !== -1 || barcodeData.text.indexOf("3S") !== -1 || barcodeData.text.indexOf("KG") !== -1){
                  swal({
                    title: "Oops...",
                    text: "PostNL codes mogen niet meer worden gescand. Gebruik een BVO zakje, of neem contact op met een beheerder.",
                    type: "error",
                    confirmButtonColor: "#009999"
                  });
                } else if (barcodeData.text.indexOf("BVO") !== -1 || barcodeData.text.indexOf("TSS") !== -1) {
                  // check of een sessie is ingesteld
                  if (localStorage.getItem("sessieKenmerk") == "") {
                    swal({
                      title: "Oops...",
                      text: "Scan eerst een sessiecode",
                      type: "error",
                      confirmButtonColor: "#009999"
                    });
                  } else {

                    var barcodeLength = barcodeData.text.length;

                    if(barcodeData.text.indexOf("BVO") !== -1 && barcodeLength != 11){
                      if(barcodeLength < 11){
                        swal({
                          title: "Oops...",
                          text:
                            "Deze barcode is te kort. Neem contact op met een beheerder",
                          type: "error",
                          confirmButtonColor: "#009999"
                        });
                      }else if(barcodeLength > 11){
                        swal({
                          title: "Oops...",
                          text:
                            "Deze barcode is te lang. Neem contact op met een beheerder",
                          type: "error",
                          confirmButtonColor: "#009999"
                        });
                      }
                    }else if(barcodeData.text.indexOf("TSS") !== -1 && barcodeLength != 12){
                      if (barcodeLength < 12) {
                        swal({
                          title: "Oops...",
                          text:
                            "Deze barcode is te kort. Neem contact op met een beheerder",
                          type: "error",
                          confirmButtonColor: "#009999"
                        });
                      } else if (barcodeLength > 12) {
                        swal({
                          title: "Oops...",
                          text:
                            "Deze barcode is te lang. Neem contact op met een beheerder",
                          type: "error",
                          confirmButtonColor: "#009999"
                        });
                      }
                    }else{
                        // 	First check if barcode already exists
                    $.ajax({
                      method: "POST",
                      dataType: "json",
                      data:
                        "data=" +
                        JSON.stringify({
                          init: {
                            module: "GetScans",
                            userId: $rootScope.loggedInUserId,
                            token: $rootScope.loggedInUserToken
                          },
                          data: {
                            RouteId: "",
                            praktijkId: "",
                            barCode: barcodeData.text,
                            scanKoerierDatum: "",
                            datumvan: "",
                            datumtot: "",
                            sessieKenmerk: "",
                            batchKenmerk: ""
                          }
                        }),
                      url: $rootScope.ajaxURL,
                      success: function(response) {
                        //Set variables
                        var kitType = barcodeData.text.slice(-1);

                        if (response.state.result == "Fail") {
                          if (
                            response.state.message == "Session Expired"
                          ) {
                            $rootScope.removeSession();
                          }

                          if (response.state.message == "No Data") {
                            //If barcode does not exist yet, create new entry

                            if (
                              $rootScope.role == "koerier" ||
                              $rootScope.role == "beheerder"
                            ) {
                              var koerierScan = 1;
                              var scanKoerierDatum =
                                $scope.dataBaseDate;
                              var laboratoriumScanDatum = "";
                              var laboratoriumScan = "";
                              var sessieKenmerk = localStorage.getItem(
                                "sessieKenmerk"
                              );
                              var batchKenmerk = localStorage.getItem(
                                "batch"
                              );

                              //Insert barcode
                              $rootScope.insertBarcode(
                                $scope.scanMonsterElement,
                                $scope.scanMonsterRouteId,
                                $scope.scanMonsterUserId,
                                barcodeData.text,
                                scanKoerierDatum,
                                koerierScan,
                                laboratoriumScanDatum,
                                laboratoriumScan,
                                kitType,
                                sessieKenmerk,
                                batchKenmerk
                              );
                            } else if (
                              $rootScope.role == "laboratorium"
                            ) {
                              swal({
                                title: "Oops...",
                                text:
                                  "Barcode is nog niet door de koerier gescand, dus niet bekend in het systeem. Neem contact op met 2MC/Commpanionz: 0341-495254.",
                                type: "error",
                                confirmButtonColor: "#009999"
                              });
                            }
                          }
                        } else if (response.state.result == "Success") {
                          //If barcode already exists, cancel if the role scanning already scanned
                          if (
                            ($rootScope.role == "koerier" &&
                              response.scans[0].koerierScan == "1") ||
                            ($rootScope.role == "beheerder" &&
                              response.scans[0].koerierScan == "1") ||
                            ($rootScope.role == "laboratorium" &&
                              response.scans[0].laboratoriumScan == "1")
                          ) {
                            swal({
                              title: "Oops...",
                              text: "Barcode is al gescand.",
                              type: "error",
                              confirmButtonColor: "#009999"
                            });
                          } else {
                            $scope.scanMonsterRouteId =
                              response.scans[0].routeId;
                            $scope.scanMonsterUserId =
                              response.scans[0].praktijkId;

                            //If barcode exists but the role scanning has not scanned yet
                            if (
                              $rootScope.role == "koerier" ||
                              $rootScope.role == "beheerder"
                            ) {
                              var koerierScan = 1;
                              var scanKoerierDatum =
                                $scope.dataBaseDate;
                              var laboratoriumScanDatum =
                                response.scans[0].laboratoriumScanDatum;
                              var laboratoriumScan =
                                response.scans[0].laboratoriumScan;
                            } else if (
                              $rootScope.role == "laboratorium"
                            ) {
                              var koerierScan =
                                response.scans[0].koerierScan;
                              var scanKoerierDatum =
                                response.scans[0].scanKoerierDatum;
                              var laboratoriumScanDatum =
                                $scope.dataBaseDate;
                              var laboratoriumScan = 1;

                              /*
															=========================================
																VERVOLG MELDING MISSENDE MONSTERS
															=========================================
														*/

                              //Add route of scanned monster to array (if it does not exist yet)
                              if (
                                scannedMonstersRoutes.indexOf(
                                  response.scans[0].routeId
                                ) === -1
                              ) {
                                scannedMonstersRoutes.push(
                                  response.scans[0].routeId
                                );
                              }
                              //Set scanned monsters routes array to JSON
                              scannedMonstersRoutes = JSON.stringify(
                                scannedMonstersRoutes
                              );
                              //Set scanned monsters routes in localStorage
                              localStorage.setItem(
                                "scannedMonstersRoutesCommpanionz",
                                scannedMonstersRoutes
                              );
                              //Set scan session to true
                              $timeout(function() {
                                $scope.scanSession = true;
                              });

                              /*
															=========================================
																EINDE MELDING MISSENDE MONSTERS
																	(wordt verderop vervolgd)
															=========================================
														*/
                            }

                            //Insert barcode
                            $rootScope.insertBarcode(
                              $scope.scanMonsterElement,
                              $scope.scanMonsterRouteId,
                              $scope.scanMonsterUserId,
                              barcodeData.text,
                              scanKoerierDatum,
                              koerierScan,
                              laboratoriumScanDatum,
                              laboratoriumScan,
                              kitType,
                              sessieKenmerk,
                              batchKenmerk
                            );
                          }
                        }
                      },
                      error: function(response) {
                        console.log(response);
                      }
                    });
                    }


                  }
                } else {
                  swal({
                    title: "Oops...",
                    text:
                      "Dat is geen correcte barcode. Controleer of de barcode begint met 'TSS' of 'BVO' of neem contact op met een beheerder",
                    type: "error",
                    confirmButtonColor: "#009999"
                  });
                }
                // } else {
                // 	swal({
                // 		  title: 'Oops...',
                // 		  text: 'U heeft nog geen sessie-barcode gescand!',
                // 		  type: 'error',
                // 		  confirmButtonColor: '#009999'
                // 		});
                // }
                //    	 else {
                // 	//If barcode is scanned and barcode is not for Commpanionz app
                // 	if ((barcodeData.text.indexOf('JBZ') == -1 || barcodeData.text.indexOf('NMDL') == -1 || barcodeData.text.indexOf('radboudumc') == -1 || barcodeData.text.indexOf('SYM') == -1 || barcodeData.text.indexOf('testregio') == -1 || barcodeData.text.indexOf('UMCG') == -1) && barcodeData.cancelled == 0) {
                // 		swal({
                // 		  title: 'Oops...',
                // 		  text: 'Dit is geen Commpanionz barcode.',
                // 		  type: 'error',
                // 		  confirmButtonColor: '#009999'
                // 		});
                // 	}
                // }
              }, function(error) {
                //If error
                console.log(error);
              });
          }
        };// ========== End scan kit or monsters functions ==========

				/*
					=========================================
						VERVOLG MELDING MISSENDE MONSTERS
					=========================================
				*/
				//Stop scan session
				$scope.stopScanSession = function() {

					//First ask if the user is sure they want to end the scan session
					swal({
					  title: 'Let op!',
					  type: 'warning',
					  html: 'Weet u zeker dat alle monsters zijn gescand en u de scansessie wilt beëindigen?',
					  confirmButtonColor: '#009999',
					  showCancelButton: true,
					  cancelButtonText: 'Sluiten',
					  confirmButtonText: 'Beëindig scansessie'
					}).then(function() {
						//Check if there is already a scan-monsters-session in the local storage
						if (localStorage.getItem('scannedMonstersRoutesCommpanionz') !== null) {
							//Set scanned monsters routes as variable
							var endScannedMonstersRoutes = JSON.parse(localStorage.getItem('scannedMonstersRoutesCommpanionz'));

							//Set missing monsters array
							var missingMonsters = [];

							//Set database date today (for date range in GetScans, to check if kits are missing)
							var dateNowStopScans = new Date();
							var currentDateStopScans = $.reverseDate(dateNowStopScans);

							//Set database date 4 days ago (for date range in GetScans, to check if kits are missing)
							var fourDaysAgoStopScans = $.reverseDate(dateNowStopScans.setDate(dateNowStopScans.getDate() - 4));

							//Check if there are monsters missing in any of the routes where monsters were scanned:
							//First loop through scanned monsters routes array
							for (var scanRoute in endScannedMonstersRoutes) {
								//Get scanned monsters for every route
								$.ajax({
									method:'POST',
									async: false,
									dataType:'json',
									data:'data=' + JSON.stringify({init:{module:"GetScans",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{RouteId:endScannedMonstersRoutes[scanRoute],praktijkId:"",barCode:"",scanKoerierDatum:"",datumvan:fourDaysAgoStopScans,datumtot:currentDateStopScans,sessieKenmerk:sessieKenmerk, batchKenmerk: batchKenmerk}}),
									url:$rootScope.ajaxURL,
									success: function(response) {
										//Loop through these scanned monsters to see if any are missing
										for (var scannedMonster in response.scans) {
											//If a monster is missing
											if (response.scans[scannedMonster].koerierScan === '1' && response.scans[scannedMonster].laboratoriumScan !== '1') {
												//If one is missing, get the name of that 'huisartsenpraktijk' + the date + the route name + kit type and set these in missingMonsters array
												missingMonsters.push({
													praktijkName: response.scans[scannedMonster].praktijkName,
													routeName: response.scans[scannedMonster].routeName,
													date: $rootScope.convertDatabaseDate(response.scans[scannedMonster].scanKoerierDatum),
													monster: response.scans[scannedMonster].kitName,
													barCode: response.scans[scannedMonster].barCode
												});
											}
										}
									}
								});
							}

							//If missing monsters exist:
							if (missingMonsters.length > 0) {

								//Mail/send message to the 'beheerders' with every 'huisartsenpraktijk' where monsters are missing
								var missingMonstersMessage = 'DIT IS EEN TEST VAN t SWARTE SCHAAP EN KAN VOLLEDIG WORDEN GENEGEERD. DEZE IS GEGENEREERD VANUIT DE TESTAPP!! Er is geconstateerd dat een of meer monsters niet bij het lab zijn aangekomen of zijn gescand: ';
								for (var singleMissingMonster in missingMonsters) {
									missingMonstersMessage = missingMonstersMessage + missingMonsters[singleMissingMonster].monster + ' met barcode ' + missingMonsters[singleMissingMonster].barCode + ' bij huisartsenpraktijk '
									+ missingMonsters[singleMissingMonster].praktijkName + ' op ' + missingMonsters[singleMissingMonster].date + ' in route '
									+ missingMonsters[singleMissingMonster].routeName + ', ';
								}

								//Give alert to the lab to notify about missing monsters
								swal({
								  title: 'Oops',
								  text: 'Er is geconstateerd dat een of meer monsters niet bij het lab zijn aangekomen of zijn gescand. Het management van 2MC/Commpanionz is op de hoogte gebracht en zal z.s.m. contact met u opnemen.',
								  type: 'warning',
								  confirmButtonColor: '#009999'
								});

								//Send message to Commpanionz to notify about missing monsters
								$.ajax({
									method:'POST',
									dataType:'json',
									data:'data=' + JSON.stringify({init:{module:"CreateMelding",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{ontvangers:'',userId:$rootScope.loggedInUserId,typemelding:'3',bericht:missingMonstersMessage,anders:'',redenklacht:'',klacht_user_id:'',klacht_van_user_id:'',routeId2send:'',regionId2send:'',roleId2send:'1',sendAllUsers:''}}),
									url:$rootScope.ajaxURL,
									success: function(response) {

										if (response.state.result == 'Fail') {
											console.log(response.state.message);

										} else if (response.state.result == 'Success') {
											console.log('Melding is verstuurd!');
										}
									},
									error: function(response) {
										console.log(response);
									}
								});//End send message to Commpanionz to notify about missing monsters

							} else {
								//if no missing monsters exist
								swal({
								  title: 'Gelukt!',
								  text: 'De gegevens zijn opgeslagen.',
								  type: 'success',
								  confirmButtonColor: '#009999'
								});
							}

							//Finally, kill the scan session by removing it from localStorage and removing the scanSession variable
							localStorage.removeItem('scannedMonstersRoutesCommpanionz');
							$scope.$apply(function() {
								delete $scope.scanSession;
							});

						} else {
							//If no scan session was found in the localStorage
							swal({
							  title: 'Oops',
							  text: 'Er is geen scansessie gevonden..',
							  type: 'warning',
							  confirmButtonColor: '#009999'
							});
						}
					});
				}//End stop scan session
				/*
					=========================================
						EINDE MELDING MISSENDE MONSTERS
					=========================================
				*/







				// ========== Delete monster scan ==========
				$scope.deleteScan = function(event,barCode,koerierScan,labScan) {

					//First ask if the user is sure they want to delete the monster scan
					swal({
					  title: 'Let op!',
					  type: 'warning',
					  html: 'Weet u zeker dat u deze scan wilt verwijderen?',
					  confirmButtonColor: '#009999',
					  showCancelButton: true,
					  cancelButtonText: 'Sluiten',
					  confirmButtonText: 'Verwijder scan'
					}).then(function() {
						//Delete scan AJAX
						$.ajax({
							method:'POST',
							dataType:'json',
							data:'data=' + JSON.stringify({init:{module:"DeleteScans",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{barCode:barCode}}),
							url:$rootScope.ajaxURL,
							success: function(response) {

								if (response.state.result == 'Fail') {
									console.log(response.state.message);

								} else if (response.state.result == 'Success') {
									//Delete scan in route from koerier (-1 in scans)
									if (koerierScan == '1') {
										var koerierScanNumber = parseInt($(event.currentTarget).parent().parent().parent().parent().parent().parent().parent().find('.scannedMonstersKoerier').text());
										$(event.currentTarget).parent().parent().parent().parent().parent().parent().parent().find('.scannedMonstersKoerier').text(koerierScanNumber - 1);
									}
									//Delete scan in route from lab (-1 in scans)
									if (labScan == '1') {
										var labScanNumber = parseInt($(event.currentTarget).parent().parent().parent().parent().parent().parent().parent().find('.scannedMonstersLab').text());
										$(event.currentTarget).parent().parent().parent().parent().parent().parent().parent().find('.scannedMonstersLab').text(labScanNumber - 1);
									}

									//Delete scan in route (delete from table)
									$(event.currentTarget).parent().parent().remove();

									swal({
									  title: 'Gelukt!',
									  text: 'Het gescande monster is verwijderd.',
									  type: 'success',
									  confirmButtonColor: '#009999'
									});
								}
							},
							error: function(response) {
								console.log(response);
							}
						});//End delete scan AJAX
					});
				}
				// ========== End delete monster scan ==========








				// ========== Open navigation ==========
				$scope.launchNavigator = function(destination) {
				    $cordovaLaunchNavigator.navigate(destination).then(function() {
				      console.log("Navigator launched");
				    }, function (err) {
				      console.error(err);
				    });
				};
				// ========== End open navigation ==========








				//Reset data on date change
				if ($scope.loggedInUserRegion != '' && $scope.loggedInUserRoute) {
					//Reset data, then load new data
					$rootScope.planningDetails = '';
					$scope.usersDetailsRoute = '';
					$scope.userAanwezigheid = '';
					$scope.getUsersAndPlanningWithRoute($scope.loggedInUserRoute);
				} else if ($scope.loggedInUserRegion == '' && $scope.loggedInUserRoute == '') {
					//Reset form on date change
					$('#routeForm')[0].reset();
					$rootScope.planningDetails = '';
					$scope.usersDetailsRoute = '';
					$scope.userAanwezigheid = '';
				}

			});
		}
	});

	//Reset all route cards and scroll to top
	$rootScope.closeRouteCards = function() {
		$('.route-card').removeClass('full-height');
		$('.toggle-route span').text('+');
		//$('.toggle-route').qtip('destroy');
		window.scrollTo(0, 0);
	}

	//toggle message overlay
	//close
	$scope.closeMessageOverlay = function() {
		$('.send-message-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	}
	//open
	$scope.openMessageOverlay = function() {
		$('.send-message-overlay').fadeIn(0);
		$('.menuContent').removeClass('nativeScroll');
	}

	//Send message
	$scope.sendMessageFunctionCall = function(routeId,regionId) {
		$timeout(function() {

			//Show loading screen
			$('.loading-overlay-extra').fadeIn(0);

			//Set message vars
			var meldingMessage = $('.melding-message').val();
			var messageType = '2';
			var routeId2send = routeId;

			//Get beheerders and add them to receivers
			$.ajax({
				method:'POST',
				dataType:'json',
				data:'data=' + JSON.stringify({init:{module:"GetUsers",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:'',roleId:'1'}}}),
				url:$rootScope.ajaxURL,
				success: function(response) {

					if (response.state.result == 'Fail') {
						swal({
						  title: 'Oops...',
						  text: response.state.message,
						  type: 'error',
						  confirmButtonColor: '#009999'
						});

						if (response.state.message == 'Session Expired') {
							$rootScope.removeSession();
						}

					} else if (response.state.result == 'Success' || response.state.result == 'Empty') {
						$timeout(function() {
							$scope.vertragingBeheerders = response.users;

							//Add beheerders to 'receivers' array
							var receivers = [];
							for (var beheerder in $scope.vertragingBeheerders) {
								receivers.push({id: $scope.vertragingBeheerders[beheerder].user_id});
							}

							//Send message
							$rootScope.sendMessage(receivers[0].id,messageType,meldingMessage,'','','','',receivers,routeId2send,'','','');

							//Hide loading screen
							$('.loading-overlay-extra').fadeOut(0);
						});

					}
				},
				error: function(response) {
					console.log(response);
				}
			});//End get beheerders and add them to receivers

		});
	}//End send message

	//Export route to CSV
	$scope.exportRouteToCsv = function() {
		$.ajax({
			method:'POST',
			dataType:'text',
			data:'data=' + JSON.stringify({init:{module:"MakeCSV",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{filter: {regionId:'',routeId:$scope.loggedInUserRoute,roleId:'2'}}}),
			url:$rootScope.ajaxURL,
			success: function(response) {
				//Force download CSV data as CSV file
				csvData = 'data:application/csv;charset=utf-8,' + encodeURIComponent(response);
		        var anchor = document.createElement('a');
				$(anchor).attr({
		            "href": csvData,
		            "download": "gebruikers_"+$.date(Date.now())+".csv"
		        });
		       	anchor.click();
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End export route to CSV




}])

//Single huisartsenpraktijk details inroute
.controller('singleUserRouteDetails', ['$scope', '$timeout', '$rootScope', function($scope, $timeout, $rootScope) {

	//Get index of praktijk in route
	$scope.routeIndex = ($scope.$index + 1) + ($scope.currentPage - 1) * $scope.pageSize + $scope.pageSize;

	//Get bestellingen for users and date in current route (per card)
	$.ajax({
		method:'POST',
		dataType:'json',
		data:'data=' + JSON.stringify({init:{module:"GetBestellingen",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:$scope.user.user_id,datum:$scope.dataBaseDate}}),
		url:$rootScope.ajaxURL,
		success: function(response) {

			if (response.state.result == 'Success') {
				$timeout(function() {
					$scope.userBestellingenRoute = response.bestellingen;
				});
			}
		},
		error: function(response) {
			console.log(response);
		}
	});//Get bestellingen for users and date in current route (per card)

	//Get SCANNED monsters per praktijk
	$scope.getSinglePraktijkScans = function() {
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"GetScans",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{RouteId:"",praktijkId:$scope.user.user_id,barCode:"",scanKoerierDatum:$scope.dataBaseDate,datumvan:"",datumtot:""}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					$timeout(function() {
						$scope.userScansRoute = [];
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					$timeout(function() {
						$scope.userScansRoute = response.scans;
					});
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	}//End get SCANNED monsters per praktijk
	$scope.getSinglePraktijkScans();

	//toggle route item
	$scope.toggleRouteItemHeight = function(event,userId,routeCardIndex) {
		$('.route-card').removeClass('full-height');
		$('.toggleRouteIcon').text('+');
		$(event.currentTarget).parents('.route-card').toggleClass('active-card');
		$(event.currentTarget).parents('.route-card').addClass('full-height');
		if ($(event.currentTarget).parents('.route-card').hasClass('active-card')) {
			$(event.currentTarget).find('span').text('-');
		} else {
			$(event.currentTarget).parents('.route-card').removeClass('full-height');
			$(event.currentTarget).find('span').text('+');
		}
		//Check if there is any check missing (geen vinkje, dus wanneer een praktijk nog niet bezocht is)
		//Geef in dat geval een melding
		$('.route-card').each(function(index) {
			if (index < routeCardIndex && routeCardIndex > 0) {
				if (!$(this).find('.check-mark')[0]) {
					swal({
					  title: 'Let op!',
					  text: 'Niet alle voorgaande praktijken zijn bezocht. Controleer of elk bezoek is doorgevoerd in de app.',
					  type: 'warning',
					  confirmButtonColor: '#009999'
					});
					return false;
				}
			} else {
				return false;
			}
		});
	}//End toggle route item

	//Save planning details for given huisartsenpraktijk
	$scope.savePlanning = function(event,kitsAfgeleverd,planningId,materiaalOpgehaald,noMateriaalGivenReason,noMateriaalGivenReasonElse,afwijkendeVerzendmethode,afwijkendeVerzendmethodeReason,notitie,praktijkId,praktijkName,routeId,regionId) {
		//Get all kits delivered
		if ($(event.currentTarget).parent().find('.kitsAfgeleverdType').val()) {
			var kitsAfgeleverdArray = [];
			$(event.currentTarget).parent().find('.kitsAfgeleverd').each(function() {
				var kitTypeAfgeleverd = $(this).find('.kitsAfgeleverdType').val();

				if ($(this).find('.kitsAfgeleverdType').val()) {

					if ($(this).find('.kitsAfgeleverdAantal').val()) {
						var kitTypeAantalAfgeleverd = $(this).find('.kitsAfgeleverdAantal').val();
					} else {
						var kitTypeAantalAfgeleverd = '0';
					}

					var kitsAfgeleverdObject = {
						KitId:kitTypeAfgeleverd,
						aantal:kitTypeAantalAfgeleverd
					}

					//Add given kits to array
					kitsAfgeleverdArray.push(kitsAfgeleverdObject);
				}
			});
		} else {
			var kitsAfgeleverdArray = '';
		}

		//Check if notitie is undefined or not (filled in or not)
		if (notitie == undefined || notitie == '') {
			notitie = '';
		} else {
			//If notitie is filled in, first remove everything between parentheses (placed on date, by...), so this can be replaced with a new name and date
			notitie = notitie.replace(/\s+\(.*?\)/g, '').replace(/\(.*?\)/g, '');
			notitie = notitie + ' (geplaatst op ' + $.date(new Date()) + ', door ' + $rootScope.username + ')';
		}

		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdatePlanning",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{planningId:planningId,datum:$.reverseDate($scope.chosenDate),routeId:routeId,praktijkId:praktijkId,afwijkendeVerzendmethode:afwijkendeVerzendmethode,afwijkendeVerzendmethodeReason:afwijkendeVerzendmethodeReason,huisartsenpraktijk:praktijkName,bestellingen:kitsAfgeleverdArray,materiaal_opgehaald:materiaalOpgehaald,kits_afgeleverd:kitsAfgeleverd,noMateriaalGivenReason:noMateriaalGivenReason,anders:noMateriaalGivenReasonElse,signatureKit:'',signatureMateriaal:'',koerier_id:$rootScope.loggedInUserId,koerier:$rootScope.username,notitie:notitie,regio:regionId}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {

					//Do not show SWAL for koeriers
					if ($rootScope.role != 'koerier') {
						//Succes message
						swal({
						  title: 'Gelukt!',
						  text: 'De gegevens zijn opgeslagen.',
						  type: 'success',
						  confirmButtonColor: '#009999'
						});
					}

					//Reset planning
					$timeout(function() {
						$rootScope.planningDetails = '';

						$timeout(function() {
							$rootScope.getPlanning($scope.dataBaseDate,'',routeId,'');
						});
					});

					//Close route card and scroll back to the top of that route card
					$(event.currentTarget).parents('.route-card').toggleClass('full-height');
					$(event.currentTarget).parents('.route-card').find('.toggleRouteIcon').text('+');
					$('.menuContent').scrollTop($('.menuContent').scrollTop() - $('.menuContent').offset().top + $(event.currentTarget).parents('.route-card').offset().top - 100);
				}
			},
			error: function(response) {
				console.log(response);
				swal({
				  title: 'Oops...',
				  text: 'Kon de gegevens niet opslaan. Ververs de pagina en probeer het nogmaals.',
				  type: 'error',
				  confirmButtonColor: '#0f7366'
				});
			}
		});
	}//End save planning details for given huisartsenpraktijk

	//Add another kit to user
	$scope.addKitToRoute = function(event) {
		$(event.currentTarget).parent().find('.kitsAfgeleverd:first').clone().find('.removeKitFromRouteButton').remove().end().appendTo($(event.currentTarget).parent().find('.kitsAfgeleverdContainer'));
	}

	//Delete route
	$scope.removeKitFromRoute = function(event) {
		$(event.currentTarget).parent().remove();
	}

	//Show scanned monsters inside a route card
	$scope.scannedMonstersInRoute = function(event) {
		$scope.getSinglePraktijkScans();
		$(event.currentTarget).parent().find('.scanned-monsters-overlay').fadeIn(0);
		$('.menuContent').removeClass('nativeScroll');
	};

	//Close scanned monsters inside a route card
	$scope.closeScannedMonstersInRoute = function(event) {
		$('.scanned-monsters-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	};

	//Edit user inside a route card
	$scope.editUserInRoute = function(event) {
		$(event.currentTarget).parent().find('.change-praktijk-overlay').fadeIn(0);
		$('.menuContent').removeClass('nativeScroll');
	};

	//Close user inside a route card
	$scope.closeEditUserInRoute = function(event) {
		$('.change-praktijk-overlay').fadeOut(0);
		$('.menuContent').addClass('nativeScroll');
	};

	$scope.saveUserInRoute = function(updateUserName,updateUserEmail,updateUserEmailNew,updateUserTelephone,updateUserAddress,updateUserCity,updateUserPostcode,notitie,userId) {

		//Check if notitie is undefined or not (filled in or not)
		if (notitie == undefined || notitie == '') {
			notitie = '';
		} else {
			//If notitie is filled in, first remove everything between parentheses (placed on date, by...), so this can be replaced with a new name and date
			notitie = notitie.replace(/\(.*?\)/g, '');
			notitie = notitie + ' (geplaatst op ' + $.date(new Date()) + ', door ' + $rootScope.username + ')';
		}
		$.ajax({
			method:'POST',
			dataType:'json',
			data:'data=' + JSON.stringify({init:{module:"UpdateUser",userId:$rootScope.loggedInUserId,token:$rootScope.loggedInUserToken},data:{userId:userId,email:updateUserEmailNew,oldemail:updateUserEmail,roleId:'',regionId:'',identity:updateUserName,name:'',lastname:'',routeId:'',routePlace:'',image:'',address:updateUserAddress,postcode:updateUserPostcode,city:updateUserCity,telephone:updateUserTelephone,standaardWaarde:'',url:'',notitie:notitie,password:'',newpassword:''}}),
			url:$rootScope.ajaxURL,
			success: function(response) {

				if (response.state.result == 'Fail') {
					swal({
					  title: 'Oops...',
					  text: response.state.message,
					  type: 'error',
					  confirmButtonColor: '#009999'
					});

					if (response.state.message == 'Session Expired') {
						$rootScope.removeSession();
					}

				} else if (response.state.result == 'Success') {
					swal({
					  title: 'Gelukt!',
					  text: 'U zult de route opnieuw moeten inladen om de wijzigingen te kunnen bekijken. Indien u geen beheerdersrol hebt zullen eventuele adreswijzigingen eerst door een beheerder moeten worden goedgekeurd.',
					  type: 'success',
					  confirmButtonColor: '#009999'
					});

					//Close user edit screen
					$scope.closeEditUserInRoute();
				}
			},
			error: function(response) {
				console.log(response);
			}
		});
	};//End edit user inside route card



}])

































;
